<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CustomersRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('theeb_customer_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id');
            $table->enum('request_type', ['OPD', 'VP'])->comment("1 : ON PREMISE DELIVERY, 2 : VEHICLE PICKUP");

            $table->string('vehicle_type')->nullable();
            $table->string('vehicle_model')->nullable();
            $table->string('vehicle_name')->nullable();
            $table->string('vehicle_number')->nullable();
            $table->bigInteger('agreement_id')->nullable();


            $table->enum('current_status', ['open','inprocess','closed']);

            $table->double('delivery_lat',8,2)->nullable();
            $table->double('delivery_long',8,2)->nullable();
            $table->string('delivery_address')->nullable();

            $table->double('pickup_lat',8,2)->nullable();
            $table->double('pickup_long',8,2)->nullable();
            $table->string('pickup_address')->nullable();

            $table->integer('request_stage')->default(1);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('theeb_customer_requests');
    }
}
