<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OdometerReadingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('theeb_odometer_reading', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('customer_request_id');
            $table->float('start_reading')->default(0);
            $table->float('end_reading')->default(0);
            $table->timestamp('start_date_time')->nullable();
            $table->timestamp('end_date_time')->nullable();

            $table->enum('customer_status', ['accepted', 'declined']);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('theeb_odometer_reading');
    }
}
