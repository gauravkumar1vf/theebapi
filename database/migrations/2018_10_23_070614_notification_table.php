<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('theeb_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('customer_request_id');
            $table->bigInteger('from_user_id');
            $table->bigInteger('to_user_id');
            $table->integer('request_stage');
            $table->text('message');
            $table->enum('sent', [0,1])->default(0);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('theeb_notifications');
    }
}
