<?php

use Illuminate\Database\Seeder;

class CustomerRequestStagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('theeb_customer_request_stages')->insert([
            [
                'name'      =>  'Onhold'
            ],
            [
                'name'      =>  'Delivery Agent'
            ],
            [
                'name'      =>  'Delivery Driver'
            ],
            [
                'name'      =>  'Assigned Customer'
            ],
            [
                'name'      =>  'Pickup Agent'
            ],
            [
                'name'      =>  'Pickup Driver'
            ]
        ]);
    }
}
