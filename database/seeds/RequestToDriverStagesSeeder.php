<?php

use Illuminate\Database\Seeder;

class RequestToDriverStagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('theeb_request_to_driver_stages')->insert([
            [
                'name'      =>  'Delivery'
            ],
            [
                'name'      =>  'Pickup'
            ],
            [
                'name'      =>  'Odometer Delivery'
            ],
            [
                'name'      =>  'Odometer Pickup'
            ]
        ]);
    }
}
