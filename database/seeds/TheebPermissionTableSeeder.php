<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TheebPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('theeb_permissions')->insert([
            [
                'name'      =>  'Pickup Call',
                'status'    =>  1
            ],
            [
                'name'      =>  'Drop Call',
                'status'    =>  1
            ],
            [
                'name'      =>  'Merge Call',
                'status'    =>  1
            ],
            [
                'name'      =>  'Close Call',
                'status'    =>  1
            ],
            [
                'name'      =>  'Assign Driver',
                'status'    =>  1
            ],
            [
                'name'      =>  'Split Call',
                'status'    =>  1
            ],
            [
                'name'      =>  'Accept Call',
                'status'    =>  1
            ]
        ]);
    }
}
