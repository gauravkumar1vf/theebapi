<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TheebRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('theeb_roles')->insert([
            [
                'name'      =>  'Customer',
                'status'    =>  1
            ],
            [
                'name'      =>  'Supervisor',
                'status'    =>  1
            ],
            [
                'name'      =>  'Agent',
                'status'    =>  1
            ],
            [
                'name'      =>  'Driver',
                'status'    =>  1
            ]
        ]);
    }
}
