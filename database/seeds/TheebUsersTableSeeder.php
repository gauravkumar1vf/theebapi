<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Faker\Factory as Faker;

class TheebUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 10) as $index) {
            DB::table('theeb_users')->insert([
                'name' => $faker->name,
                'phone' => '1234567890',
                'email' => $faker->unique()->email,
                'username' => $faker->unique()->username,
                'password' => Hash::make('123456'),
                'role_id' => $faker->randomElement([
                    1,2,3,4
                ]),
                'created_by' => '0',
                'status' => 1
            ]);
        }
    }
}
